package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IProjectTaskEndpoint getProjectTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

}