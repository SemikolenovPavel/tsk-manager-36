package ru.t1.semikolenov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.semikolenov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.semikolenov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.semikolenov.tm.api.service.IPropertyService;
import ru.t1.semikolenov.tm.dto.request.*;
import ru.t1.semikolenov.tm.dto.response.*;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.marker.ISoapCategory;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.service.PropertyService;
import ru.t1.semikolenov.tm.util.DateUtil;

import java.util.Date;

@Category(ISoapCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Nullable
    private Task taskBefore;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(
                new TaskCreateRequest(token, "test_one", "test_one", null, null)
        );
        taskBefore = createResponse.getTask();
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(null, taskBefore.getId(), Status.COMPLETED)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(token, taskBefore.getId(), null)));
        TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(token, taskBefore.getId(), Status.COMPLETED));
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertNotEquals(taskBefore.getStatus(), task.getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(null, 0, Status.COMPLETED)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(token, 0, null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(token, 100500, Status.COMPLETED)));
        TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(
                new TaskChangeStatusByIndexRequest(token, 0, Status.COMPLETED));
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertNotEquals(taskBefore.getStatus(), task.getStatus());
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(null, "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(token, "", "", null, null)));
        @NotNull final String taskName = "test_name";
        @NotNull final String taskDescription = "test_description";
        @NotNull final Date dateBegin = DateUtil.toDate("10.10.2021");
        @NotNull final Date dateEnd = DateUtil.toDate("11.11.2021");
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, taskName, taskDescription, dateBegin, dateEnd));
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertEquals(taskName, task.getName());
    }

    @Test
    public void removeTaskByIdTest() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(null, taskBefore.getId())));
        taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, taskBefore.getId()));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(token, null)).getTasks());
    }

    @Test
    public void removeTaskByIndexTest() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(null, 0)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(token, 100500)));
        taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(token, 0));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(token, null)).getTasks());
    }

    @Test
    public void showTaskById() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskById(new TaskShowByIdRequest(null, taskBefore.getId())));
        @NotNull final TaskShowByIdResponse response = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, taskBefore.getId()));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void showTaskByIndex() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskByIndex(new TaskShowByIndexRequest(null, 0)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskByIndex(new TaskShowByIndexRequest(token, 100500)));
        @NotNull final TaskShowByIndexResponse response = taskEndpoint.showTaskByIndex(
                new TaskShowByIndexRequest(token, 0));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(null, taskBefore.getId(), "", "")));
        @NotNull final TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(token, taskBefore.getId(), "new_name", "new_description"));
        Assert.assertNotNull(response);
        @Nullable Task Task = response.getTask();
        Assert.assertNotNull(Task);
        Assert.assertNotEquals(taskBefore.getName(), Task.getName());
    }

    @Test
    public void updateTaskByIndex() {
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(null, 0, "", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskByIndex(
                        new TaskUpdateByIndexRequest(token, 100500, "", "")));
        @NotNull final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest(token, 0, "new_name", "new_description"));
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertNotNull(task);
        Assert.assertNotEquals(taskBefore.getName(), task.getName());
    }

}
