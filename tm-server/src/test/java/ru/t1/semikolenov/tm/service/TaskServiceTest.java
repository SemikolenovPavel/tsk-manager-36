package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.service.ITaskService;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.entity.ModelNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.repository.TaskRepository;
import ru.t1.semikolenov.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private static final String USER_ID_TEST_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_TEST_2 = UUID.randomUUID().toString();

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        taskRepository.create(USER_ID_TEST_1, "test_1");
        taskRepository.create(USER_ID_TEST_1, "test_2");
        taskRepository.create(USER_ID_TEST_2, "test_3");
        taskRepository.create(USER_ID_TEST_2, "test_4");
        taskRepository.create(USER_ID_TEST_2, "test_5");
        INITIAL_SIZE = taskRepository.getSize();
    }

    @Test
    public void create() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.create("", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> taskService.create(USER_ID_TEST_1, ""));
        taskService.create(USER_ID_TEST_1, "test");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.create("", "test", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> taskService.create(USER_ID_TEST_1, "", "test"));
        Assert.assertThrows(EmptyDescriptionException.class, () -> taskService.create(USER_ID_TEST_1, "test", ""));
        taskService.create(USER_ID_TEST_1, "test", "test");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
    }

    @Test
    public void createWithDescriptionAndDate() {
        @NotNull final Task task = taskService.create(
                USER_ID_TEST_1,
                "test",
                "test",
                DateUtil.toDate("01.01.2020"),
                DateUtil.toDate("01.01.2021")
        );
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
        Assert.assertNotNull(task.getDateBegin());
        Assert.assertNotNull(task.getDateEnd());
    }

    @Test
    public void clear() {
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasksAll = taskService.findAll();
        Assert.assertEquals(INITIAL_SIZE, tasksAll.size());
        @NotNull final List<Task> tasksOwnedUser2 = taskService.findAll(USER_ID_TEST_2);
        Assert.assertEquals(3, tasksOwnedUser2.size());
        @NotNull final List<Task> tasksUserEmpty = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, tasksUserEmpty.size());
    }

    @Test
    public void updateById() {
        @NotNull final Task task = taskService.findOneByIndex(0);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyUserIdException.class,
                () -> taskService.updateById("", taskId, "test", "test"));
        Assert.assertThrows(EmptyIdException.class,
                () -> taskService.updateById(USER_ID_TEST_1, "", "test", "test"));
        Assert.assertThrows(EmptyNameException.class,
                () -> taskService.updateById(USER_ID_TEST_1, taskId, "", "test"));
        Assert.assertThrows(ModelNotFoundException.class,
                () -> taskService.updateById(USER_ID_TEST_1, "task_id", "test", "test"));
        @NotNull final String name = "name";
        @NotNull final String description = "description";
        taskService.updateById(USER_ID_TEST_1, taskId, name, description);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final Integer taskIndex = 0;
        @NotNull final Task task = taskService.findOneByIndex(taskIndex);
        Assert.assertThrows(EmptyUserIdException.class,
                () -> taskService.updateByIndex("", taskIndex, "test", "test"));
        Assert.assertThrows(EmptyNameException.class,
                () -> taskService.updateByIndex(USER_ID_TEST_1, taskIndex, "", "test"));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> taskService.updateByIndex(USER_ID_TEST_1, -1, "test", "test"));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> taskService.updateByIndex(USER_ID_TEST_1, 100500, "test", "test"));
        @NotNull final String name = "name";
        @NotNull final String description = "description";
        taskService.updateByIndex(USER_ID_TEST_1, taskIndex, name, description);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Task task = taskService.findOneByIndex(0);
        @NotNull final String taskId = task.getId();
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(EmptyUserIdException.class,
                () -> taskService.changeTaskStatusById("", taskId, newStatus));
        Assert.assertThrows(EmptyIdException.class,
                () -> taskService.changeTaskStatusById(USER_ID_TEST_1, "", newStatus));
        Assert.assertThrows(ModelNotFoundException.class,
                () -> taskService.changeTaskStatusById(USER_ID_TEST_1, "task_id", newStatus));
        taskService.changeTaskStatusById(USER_ID_TEST_1, taskId, newStatus);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(newStatus, task.getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() {
        @NotNull final Integer taskIndex = 0;
        @NotNull final Task task = taskService.findOneByIndex(taskIndex);
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(EmptyUserIdException.class,
                () -> taskService.changeTaskStatusByIndex("", taskIndex, newStatus));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> taskService.changeTaskStatusByIndex(USER_ID_TEST_1, -1, newStatus));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> taskService.changeTaskStatusByIndex(USER_ID_TEST_1, 100500, newStatus));
        taskService.changeTaskStatusByIndex(USER_ID_TEST_1, taskIndex, newStatus);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(newStatus, task.getStatus());
    }

    @Test
    public void findOneById() {
        @NotNull final String taskName = "test_find_id";
        @NotNull final Task task = taskService.create(USER_ID_TEST_1, taskName);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyIdException.class, () -> taskService.findOneById(""));
        Assert.assertNotNull(taskService.findOneById(taskId));
        Assert.assertEquals(taskName, taskService.findOneById(taskId).getName());
        Assert.assertThrows(ModelNotFoundException.class,
                () -> taskService.findOneById(UUID.randomUUID().toString()));
        Assert.assertNotNull(taskService.findOneById(USER_ID_TEST_1, taskId));
        Assert.assertEquals(taskName, taskService.findOneById(USER_ID_TEST_1, taskId).getName());
        Assert.assertThrows(ModelNotFoundException.class,
                () -> taskService.findOneById(USER_ID_TEST_1, UUID.randomUUID().toString()));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final String taskName = "test_find_index";
        taskService.create(USER_ID_TEST_1, taskName);
        @NotNull final Integer taskIndex = 2;
        Assert.assertThrows(IncorrectIndexException.class,
                () -> taskService.findOneByIndex(USER_ID_TEST_1, -1));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> taskService.findOneByIndex(USER_ID_TEST_1, 100500));
        Assert.assertNotNull(taskService.findOneByIndex(USER_ID_TEST_1, taskIndex));
        Assert.assertEquals(taskName, taskService.findOneByIndex(USER_ID_TEST_1, taskIndex).getName());
    }

    @Test
    public void existsById() {
        @NotNull final String taskName = "test_exist_id";
        @NotNull final Task task = taskService.create(USER_ID_TEST_1, taskName);
        @NotNull final String taskId = task.getId();
        Assert.assertTrue(taskService.existsById(taskId));
        Assert.assertFalse(taskService.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        @NotNull final Task task = taskService.create(USER_ID_TEST_1, "test");
        @NotNull final String taskId = task.getId();
        taskService.remove(task);
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(taskId));
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
        taskService.add(task);
        taskService.remove(USER_ID_TEST_1, task);
        Assert.assertThrows(ModelNotFoundException.class,
                () -> taskService.findOneById(USER_ID_TEST_1, taskId));
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
    }

    @Test
    public void removeById() {
        @NotNull final Task task = taskService.create(USER_ID_TEST_1, "test");
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyIdException.class, () -> taskService.removeById(""));
        taskService.removeById(taskId);
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(taskId));
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
        taskService.add(task);
        Assert.assertThrows(EmptyIdException.class, () -> taskService.removeById(USER_ID_TEST_1, ""));
        taskService.removeById(USER_ID_TEST_1, taskId);
        Assert.assertThrows(ModelNotFoundException.class,
                () -> taskService.findOneById(USER_ID_TEST_1, UUID.randomUUID().toString()));
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
    }

    @Test
    public void removeByIndex() {
        taskService.create(USER_ID_TEST_1, "test");
        @NotNull final Integer taskIndex = 2;
        Assert.assertThrows(IncorrectIndexException.class,
                () -> taskService.removeByIndex(USER_ID_TEST_1, -1));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> taskService.removeByIndex(USER_ID_TEST_1, 1000));
        taskService.removeByIndex(USER_ID_TEST_1, taskIndex);
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
    }

}
