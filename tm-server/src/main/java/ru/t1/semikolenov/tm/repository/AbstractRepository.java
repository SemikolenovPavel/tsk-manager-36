package ru.t1.semikolenov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.IRepository;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new Vector<>();

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        records.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        this.records.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return records.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return records.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return records.get(index);
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public long getSize() {
        return records.size();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        records.removeAll(collection);
    }

}