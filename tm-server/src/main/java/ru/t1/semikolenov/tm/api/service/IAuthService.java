package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.model.Session;
import ru.t1.semikolenov.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}