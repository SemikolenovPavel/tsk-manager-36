package ru.t1.semikolenov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.endpoint.*;
import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.repository.IUserRepository;
import ru.t1.semikolenov.tm.api.service.*;
import ru.t1.semikolenov.tm.dto.request.*;
import ru.t1.semikolenov.tm.endpoint.*;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.User;
import ru.t1.semikolenov.tm.repository.ProjectRepository;
import ru.t1.semikolenov.tm.repository.TaskRepository;
import ru.t1.semikolenov.tm.repository.UserRepository;
import ru.t1.semikolenov.tm.service.*;
import ru.t1.semikolenov.tm.util.DateUtil;
import ru.t1.semikolenov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Backup backup = new Backup(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final CalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(calculatorEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void initDemoData() {
        @NotNull final User testUser = userService.create("test", "test", "test@test.ru");
        @NotNull final User adminUser = userService.create("admin", "admin", Role.ADMIN);
        adminUser.setId("0d6a81fb-cd45-4b8a-a390-d16f87bc259d");
        projectService.create(
                testUser.getId(), "DEMO-PROJECT-1", "DEMO PROJECT 1",
                DateUtil.toDate("01.01.2020"), DateUtil.toDate("01.01.2021")).setStatus(Status.IN_PROGRESS);
        projectService.create(
                testUser.getId(), "DEMO-PROJECT-2", "DEMO PROJECT 2",
                DateUtil.toDate("01.02.2020"), DateUtil.toDate("01.01.2021")).setStatus(Status.COMPLETED);
        projectService.create(
                testUser.getId(), "DEMO-PROJECT-3", "DEMO PROJECT 3",
                DateUtil.toDate("01.03.2020"), DateUtil.toDate("01.01.2021")).setStatus(Status.IN_PROGRESS);
        projectService.create(
                adminUser.getId(), "DEMO-PROJECT-4", "DEMO PROJECT 4",
                DateUtil.toDate("01.04.2020"), DateUtil.toDate("01.01.2021")).setStatus(Status.NOT_STARTED);
        taskService.create(
                testUser.getId(), "DEMO-TASK-1", "DEMO TASK 1",
                DateUtil.toDate("01.05.2020"), DateUtil.toDate("01.01.2021")).setStatus(Status.IN_PROGRESS);
        taskService.create(
                adminUser.getId(), "DEMO-TASK-2", "DEMO TASK 2",
                DateUtil.toDate("01.06.2020"), DateUtil.toDate("01.01.2021")).setStatus(Status.NOT_STARTED);
        taskService.create(
                adminUser.getId(), "DEMO-TASK-3", "DEMO TASK 3",
                DateUtil.toDate("01.07.2020"), DateUtil.toDate("01.01.2021")).setStatus(Status.NOT_STARTED);
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}