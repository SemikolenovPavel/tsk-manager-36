package ru.t1.semikolenov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class CalculatorEndpoint {

    @WebMethod
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }

}
